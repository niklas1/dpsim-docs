---
title: "Nodal Analysis"
date: 2020-02-28T22:17:54Z
markup: pandoc
draft: true
---

<script src="https://polyfill.io/v3/polyfill.min.js?features=es6"></script>
<script id="MathJax-script" async src="https://cdn.jsdelivr.net/npm/mathjax@3/es5/tex-mml-chtml.js"></script>

## Theoretical Background

A circuit with $b$ branches has $2b$ unknowns since there are $b$ voltages and $b$ currents.
Hence, $2b$ linear independent equations are required to solve the circuit.
If the circuit has $n$ nodes and $b$ branches, it has

* Kirchoff's current law (KCL) equations
* Kirchoff's voltage law (KVL) equations
* Characteristic equations (Ohm's Law)

There are only $n-1$ KCLs since the nth equation is a linear combination of the remaining $n-1$.
At the same time, it can be demonstrated that if we can imagine a very high number of closed paths in the network, only $b-n+1$ are able to provide independent KVLs.
Finally there are $b$ characteristic equations, describing the behavior of the branch, making a total of $2b$ linear independent equations.

The nodal analysis method reduces the number of equations that need to be solved simultaneously.
$n-1$ voltage variables are defined and solved, writing $n-1$ KCL based equations.
A circuit can be solved using Nodal Analysis by applying the following steps:

* Select a reference node (mathematical ground) and number the remaining $n-1$ nodes, that are the independent voltage variables
* Represent every branch current $i$ as a function of node voltage variables $v$ with the general expression $i = g(v)$
* Write $n-1$ KCL based equations in terms of node voltage variable.

The resulting equations can be written in matrix form and have to be solved for $v$.
$$
	\boldsymbol{Y} \boldsymbol{v} = \boldsymbol{i}
$$

### Generating the System Matrix
The matrix stamp concept is an approach to construct the matrix equation.
Each element of the circuit will stamp the matrix equation in a certain position.

### Resistor
A resistor $R$ between the nodes m and n will stamp the conductance matrix $\boldsymbol{Y}$ in the positions $m,m$ and $n,n$ with the value $\frac{1}{R}$ and in the positions $m,n$ and $n,m$ with the value $-\frac{1}{R}$.

### Current Source
A current source of $I_{src}$ Ampere entering node $j$ and leaving node $k$ will stamp the vector at line $j$ with $I_{src}$ and at line $k$ with $-I_{src}$.

## Modified Nodal Analysis
An ideal voltage source cannot be included in nodal analysis. In this case, the modified nodal analysis approach is used.
The voltage source is represented by adding a new equation to the problem and adding the current through the voltage source $i_{jk}$ as unknown.
For a voltage source with positive terminal connected to node $j$ and negative terminal connected to node $k$, the added equation is the following:
$$
  v_m - v_n = V_{src}
$$
Moreover, the new unknown $i_{jk}$ is added to the equation of node $j$ and subtracted to the equation of node $k$.

Components such as capacitor, inductor and real voltage source (ideal voltage source in series with a resistance) can be represented by equivalent circuits, composed by a current source and a resistance.
That means that the matrix stamp of these components is made by stamping a current source and a resistance on the matrix equation.


## Resistive Companion Method

### Inductor
The presence of an inductor or a capacitor in a circuit introduces dynamics that require the knowledge of the "past" behavior of the component to represent its quantities at a given time.
An inductor can be described by the following equations:
$$
  v_L(t) = L \frac{di_L(t)}{dt}
$$
$$
  i_L(t) = i_L(t - \Delta t) + \frac{1}{L} \int_{t - \Delta t}^{t} v_L(\tau) d \tau
$$

A numerical solution for these equations can only be obtained at a finite number of points, therefore a discretization of time is required.

For modeling inductors and capacitors, resistive companion method is used.
Each dynamic component is transformed into a DC equivalent circuit, which represent one iteration of an integration method.
There are different integration methods available to solve the integration problem. The method chosen in this work is the trapezoidal rule.

Given the following equation, that defines the value of a variable $y$ at the point $k+1$.
$$
	y(k+1) = y(k) + \int_{t_k}^{t_k+ \Delta t} x(\tau)d \tau
$$
With the trapezoidal rule $y(k+1)$ is calculated as following:

$$
	y(k+1)=y(k)+ \frac{x(k)+x(k+1)}{2} \cdot \Delta t
$$

### Capacitor

The same approach used for the inductor is used now for the capacitor.
A capacitor can be described by the following equations:

$$
	i_C(t) = C \frac{d v_c}{dt}
$$

$$
	v_C(t)= v_C(t - \Delta t) + \frac{1}{C} \int_{t - \Delta t}^{t} i_C (\tau) d \tau
$$

## Introducing Dynamic Phasors

### Inductor

In dynamic phasors the inductor equations become:
$$
  v_L(t) = L \frac{di_L(t)}{dt} + j \omega \cdot L \cdot  i_L(t)
$$

$$
  i_L(t) = i_L(t- \Delta t) +  \int_{t- \Delta t}^{t} \frac{1}{L} \cdot v_L(\tau) -j \omega \cdot i_L(\tau)d \tau
$$

Applying trapezoidal rule for the inductor equation in dynamic phasor:
$$
  i_L(k+1) = i_L(k) + \frac{\Delta t}{2} \left[ \frac{1}{L} \cdot v_L(k+1) - j \omega \cdot i_L(k+1) + \frac{1}{L} \cdot v(k) - j \omega \cdot i_L(k) \right]
$$

Defining:
$$
\begin{align}
	a &= \frac{\Delta t}{2L} \\
	b &= \frac{\Delta t \omega}{2}
\end{align}
$$
The equation can be rewritten as
$$
\begin{align}
  i_L(k+1) &= i_L(k) + a \cdot v_L(k+1) - j b \cdot i_L(k+1) + a \cdot v(k) - j b \cdot i_L(k) \\
  i_L(k+1) &= \frac{1-b^2-j2b}{1+b^2}i_L(k) + \frac{a-jab}{1+b^2} v_L(k) + \frac{a-jab}{1+b^2} v_L(k+1)
\end{align}
$$
The equation shows that the inductor current at the step k+1 has a part which depends on the voltage and current at the step k and a part which depends on the actual voltage. Therefore, the inductor can be substituted with a resistance $R_L$ in parallel with a current source $A_L(k)$. The value of $R_L$ is constant for a constant frequency and constant time step. The value of $A_L(k)$ depends on the knowledge of the previous step and has to be updated for each iteration.

$$
	I_{L,equiv}(k) = i_L(k) \cdot \frac{1-b^2-2bj}{1+b^2} + v_L(k) \cdot \frac{a-jab}{1+b^2}
$$

$$
	R_L = \frac{1+b^2}{a-jab}
$$


### Capacitor

In dynamic phasor the capacitor equations become:

$$
  i_C(t)= C \frac{d v_c}{dt} + j \omega \cdot C \cdot v_C(t)
$$

$$
  v_C(t) = v_C(t- \Delta t) +  \int_{t- \Delta t}^{t} \frac{1}{C} \cdot i_C(\tau) -j \omega \cdot v_C(\tau)d \tau
$$

Applying trapezoidal rule for the capacitor equation:

$$
  v_C(k+1) = v_C(k) + \frac{\Delta t}{2} \left[ \frac{1}{C} \cdot i_C(k+1) - j \omega \cdot v_C(k+1) + \frac{1}{C} \cdot i_C(k) - j \omega \cdot v_C(k) \right]
$$

Defining:
$$
\begin{align}
  a &= \frac{\Delta t}{2C} \\
  b &= \frac{\Delta t \omega}{2}
\end{align}
$$
$$
\begin{align}
  v_C(k+1) &= v_C(k) + a \cdot i_C(k+1) - j b \cdot v_C(k+1) + a \cdot i_C(k) - j b \cdot v_C(k) \\
  i_C(k+1) &= \frac{1+jb}{a} \cdot v_C(k+1) - \frac{1-jb}{a} \cdot v_C(k) -i_C(k)
\end{align}
$$
The capacitor in calculation step $k+1$ can also be substituted with a resistance $R_C$ in parallel with a current source $I_{C,equiv}(k)$.
The value of $R_C$ is constant for a constant frequency and constant time step.
The value of $I_{C,equiv}(k)$ depends only on the previous time step and changes for each iteration.

$$
	I_{C,equiv} = -\frac{1-jb}{a} v_C(k) + -i_C(k)
$$

$$
	R_C = \frac{a}{1+jb}
$$