---
title: "Branches"
date: 2020-02-28T22:17:54Z
markup: pandoc
draft: true
---

<script src="https://polyfill.io/v3/polyfill.min.js?features=es6"></script>
<script id="MathJax-script" async src="https://cdn.jsdelivr.net/npm/mathjax@3/es5/tex-mml-chtml.js"></script>

## RX-Line
The RX Line model corresponds to a resistance in series with a inductance. The inductance is represented using resistive companion and therefore replaced by a resistance in parallel to a current source.

* name: Name of the component
* node1: First node, where the resistance is connected
* node2: Second node, where the inductance is connected
* node3: Node between resistance and inductance
* resistance: Resistance in ohms
* inductance: Inductance in H

In this case, the function "applySystemMatrixStamp" will stamp the resistances $R$ between node1 and node3 and $R_L$ between node3 and node2 to the conductance matrix $G$. The function "step" will recalculate the value of $A_L$ as in equation \ref{eq:AL} and stamp it between node3 and node2 to the vector $A$. The function "poststep" will update the values of inductor current and voltage using equations \ref{eq:inductorCurrent} and \ref{eq:inductorVoltage}.

The second way of define the RX Line is defining only two nodes (node1 and node2). In this case, the equivalent circuit in figure \ref{fig:RxLine2} is considered.

The function "applySystemMatrixStamp" will stamp the resistance $R+R_L$ between node1 and node2 to the conductance matrix $G$.

The function "step" will update the values of $A_L$ as in equation \ref{eq:AL} and $A_{Leq}(k)$. $A_{Leq}$ will be stamped to the vector $A$ as in equation \ref{eq:AleqStamp}.

\begin{equation}
  A_{Leq}(k)=A_L(k) \cdot \frac{R_L}{R_L+R}
\end{equation}

The function "poststep" will calculate the values of the line voltage and current and inductor voltage and current for the actual step (k+1).

\begin{equation}
	v_{Line}(k+1) = v_{node1}(k+1) - v_{node2}(k+1)
\end{equation}

\begin{equation}
	i_{Line}(k+1) = \frac{v_{Line}(k+1)}{R+R_L} + A_{Leq}
\end{equation}

\begin{equation}
	v_L(k+1) = v_{Line}(k+1) - R \cdot i_{Line}(k+1)
\end{equation}

\begin{equation}
	i_L(k+1) = i_{Line}(k+1)
\end{equation}

In both cases, the function "init" is setting every initial value to zero.

## PI-Line
The Pi Line model corresponds to a resistance in series with a inductance and two shunt capacitors as shown in figure \ref{fig:PiLine}. The inductance and the capacitance are represented using resistive companion and therefore replaced by a resitance in parallel to a current source.

The following parameters are defined by the user:
* name: Name of the component
* node1: First node, where the resistance and the first capacitor are connected
* node2: Second node, where the inductance and the second capacitor are connected
* node3: Node between resistance and inductance
* resistance: Resistance in ohms
* inductance: Inductance in H
* capacitance: Capacitance in F

The function "applySystemMatrixStamp" will stamp the resistances $R$ between node1 and node3, $R_L$ between node3 and node2, $R_{C1}$ between node1 and reference node and $R_{C2}$ between node2 and reference node to the conductance matrix $G$.

The function "step" will recalculate the value of $A_L$, $A_{C1}$ and $A_{C2}$ and stamp it to the vector $A$.

The function "poststep" will update the values of inductor and capacitors current and voltage.

## Transformer

### Typical Parameters


\begin{equation}
	Z = u_{kr} \cdot \frac{U_{r1}^2}{S_{r}}
\end{equation}

\begin{equation}
	R = u_{Rr} \cdot \frac{U_{r1}^2}{S_{r}}
\end{equation}

\begin{equation}
	X = \sqrt{Z^2-R^2}
\end{equation}

\begin{equation}
	R_{Fe} = \frac{U_{r1}^2}{P_{Fe}}
\end{equation}

\begin{equation}
	X_{0} = \frac{U_{r1}}{\sqrt{3} I_{0}}
\end{equation}


### Nodal Analysis Representation

The ideal transformer has an inner node which requires an extension of the system matrix. $j$ is the high voltage node while $k$ is the low voltage node. $l$ is the inner node of the transformer. The transformer ration is defined as $T=V_{j}/V_{k}$. A phase shift can be introduced if $T$ is considered as complex number.